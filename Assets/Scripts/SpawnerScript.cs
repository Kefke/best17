using System.Collections.Generic;
using UnityEngine;


public class SpawnerScript : MonoBehaviour
{
    public GameObject prototype = null;
    public int max_spawned_objects = 1;
    public float time_between_spawns = 2f;
    public float spawn_radius = 100f;
    public float clear_radius = 5f;

    private readonly List<GameObject> spawned = new List<GameObject>();
    private float timer = 0f;

    private void Update()
    {
        if ( timer > 0f )
        {
            timer -= Time.deltaTime;
            return;
        }
        if ( Spawn() )
            timer = time_between_spawns;
    }

    private bool Spawn()
    {
        if ( !prototype )
            return false;
        spawned.RemoveAll( x => x == null );
        if ( spawned.Count >= max_spawned_objects )
            return false;
        // Calculate random position
        Vector3 position = transform.position;
        position.x += (Random.value - .5f) * spawn_radius * 2;
        position.z += (Random.value - .5f) * spawn_radius * 2;
        // Check if position is free
        Collider[] hitColliders = Physics.OverlapSphere(position, clear_radius);
        for (int i = 0; i < hitColliders.Length; ++i)
        {
            if (hitColliders[i].tag == "Player" || hitColliders[i].tag == "Enemy")
            {
                Debug.Log("Could not instantiate Enemy: position not free.");
                return false;
            }
        }
        // Instatiate and save
        GameObject go = Instantiate( prototype, position, transform.rotation );
        spawned.Add( go );
        return true;
    }
}
