﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathScreenScript : MonoBehaviour
{
    public void Again()
    {
        SceneManager.LoadScene("Sc1");
    }

    public void BackToMain()
    {
        SceneManager.LoadScene("mainmenu");
    }
}
