using UnityEngine;
using UnityEngine.AI;


public class EnemyScript : MonoBehaviour
{
  private const string ANIMATION_DEATH = "Die";
  private Animator anim = null;
  private bool isHit = false;

  private void Awake()
  {
    anim = GetComponent<Animator>();
  }

  private void Hit()
  {
    if (isHit) return;
    isHit = true;
    GameMainScript.EnemyKilled();
    if ( anim )
    {
      anim.Play( ANIMATION_DEATH );
      NavMeshAgent nav_mesh_agent = GetComponent<NavMeshAgent>();
      if( nav_mesh_agent )
        Destroy( nav_mesh_agent );
    }
    else
      Die();
  }

  private void Die()
  {
    Destroy( gameObject );
  }
}
