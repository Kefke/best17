using UnityEngine;


public class LaserScript : MonoBehaviour
{
  private RaycastHit hit;
  private float range = 100.0f;
  private LineRenderer line;
  public GameObject laser_end = null;

  public static float energy = 3f;
  public const float MAX_ENERGY = 3f;
  public bool on_recharge = false;

  private void Start()
  {
    line = GetComponent<LineRenderer>();
    line.numPositions = 2;
    energy = 3f;
  }

  private void Update()
  {
    if ( Input.GetButton( "Fire2" ) && energy > 0f && !on_recharge )
    {
      line.enabled = true;
      line.SetPosition( 0, transform.position );
      Vector3 end_point;
      Ray ray = new Ray( transform.position, transform.forward ); // creating ray, pointing forward for laser
      if ( Physics.Raycast( ray, out hit, range ) ) // finding a laser hit point
      {
        end_point = hit.point;
        hit.transform.SendMessage( "Hit", SendMessageOptions.DontRequireReceiver );
      }
      else
        end_point = transform.position + transform.forward * range;
      line.SetPosition( 1, end_point );
      if ( laser_end )
      {
        laser_end.transform.position = end_point;
        laser_end.SetActive( true );
      }
      energy -= Time.deltaTime / 2;
    }
    else
    {
      line.enabled = false;
      if ( laser_end )
        laser_end.SetActive( false );
      energy += Time.deltaTime / 4;
      energy = Mathf.Clamp( energy, 0, MAX_ENERGY );
      on_recharge = energy < 1f;
    }
  }
}
