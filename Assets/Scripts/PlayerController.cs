using UnityEngine;


public class PlayerController : MonoBehaviour
{
    private const string ANIMATOR_SPEED = "Speed";
    private const string ANIMATOR_ATTACK = "Attack";
    private const string ANIMATOR_ALIVE = "Alive";
    private const string ANIMATION_DEATH = "Die";
    public float movement_speed = 0.5f;
    public float rotation_speed = 2f;
    public GameObject deathScreen = null;
    private Animator anim;
    private Rigidbody rb;
    private bool isAlive = true;

    // Start called once for object (after Awake)
    // To learn more https://docs.unity3d.com/Manual/ExecutionOrder.html
    private void Start()
    {
        anim = GetComponent<Animator>(); // Geting component of type "Animator" to use it later
        rb = GetComponent<Rigidbody>(); // Geting component of type "RigidBody" to use it later
    }

    // FixedUpdate called once per fixed time. check Edit->ProjectSettings->Time
    private void FixedUpdate()
    {
        // Check if alive
        if (!isAlive)
            return;
        // getting controller input value for axises
        float h = Input.GetAxis( "Horizontal" );
        float v = Input.GetAxis( "Vertical" );

        bool attack = Input.GetButton("Fire1");

        anim.SetFloat( ANIMATOR_SPEED, v ); // setting animator parameter
        //anim.GetFloat( ANIMATOR_SPEED ); // to get float param. use anim.Get... to get parameters of other types
        anim.SetBool(ANIMATOR_ATTACK, attack); // setting animator parameter

        Vector3 translation = transform.forward * v * movement_speed; // define movement direction and value

        rb.MovePosition( transform.position + translation ); // move character useing rigid body (to get physics to work)
        transform.Rotate( Vector3.up, h * rotation_speed ); // rotate character around his Y axis
    }

    // The functions for animation events
    private void Fire()
    {
        Debug.Log("FIRE!!!!");
    }

    private void PlayerDie()
    {
        Debug.Log("DEAD!!!!");
        anim.Stop();
        deathScreen.SetActive(true);
    }

    // Collision checks
    private void OnCollisionEnter(Collision collision)
    {
        if (!isAlive)
            return;
        //Debug.Log("Touched " + collision.gameObject.tag);
        if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log("You've died!");
            isAlive = false;
            anim.Play(ANIMATION_DEATH);
            //anim.SetBool(ANIMATOR_ALIVE, isAlive);
        }
    }
}
